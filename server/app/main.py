import json

from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
from fastapi import FastAPI, HTTPException

app = FastAPI()

JSON_DB = '/code/app/data.json'


class Issue(BaseModel):
    id: str
    title: str
    description: str


with open(JSON_DB, 'r') as f:
    data = json.load(f)["issues"]

ISSUE = {
    "id": 1,
    "title": "your app broke my pc!",
    "description": "my Google disappeared."
}


def flush_data():
    json_object = json.dumps(data, indent=4)
    with open(JSON_DB, 'w') as f:
        f.write(json_object)


@app.get("/issues/{issue_id}")
def read_issue(issue_id: str):
    if issue_id in data:
        return data[issue_id]
    else:
        raise HTTPException(status_code=404, detail="Issue not found")


@app.post("/issues/")
def create_issue(issue: Issue):
    if not issue.id in data:
        jsonable_issue = jsonable_encoder(issue)
        data[issue.id] = jsonable_issue
        flush_data()
        return issue
    else:
        raise HTTPException(status_code=400, detail="Issue already exists.")


@app.put("/issues/{issue_id}")
def update_issue(issue_id: str, issue: Issue):
    if issue_id in data:
        jsonable_issue = jsonable_encoder(issue)
        data[issue.id] = jsonable_issue
        flush_data()
        return issue
    else:
        raise HTTPException(status_code=404, detail="Issue not found")


@app.delete("/issues/{issue_id}")
def delete_issue(issue_id: str):
    if issue_id in data:
        data.pop(issue_id)
        flush_data()
        return issue_id
    else:
        raise HTTPException(status_code=404, detail="Issue not found")
