const axios = require('axios')
fs = require('fs');

const HELP = `Usage: client OPERATION [ARG]

Operation can be one of the following:
        create [JSON_FILE]
        read [ID]
        update [JSON_FILE]
        delete [ID]`

const REST_URL = 'http://localhost'

const args = process.argv.slice(2)

if (!args[0]) {
    print_help(true)
    process.exit()
}

command = args[0].toLowerCase()

switch (command) {

    case "read":
        id = args[1]
        if (typeof id === 'undefined') {
            print_help()
        } else {
            read(id)
        }
        break;

    case "create":
        json_file = args[1]
        if (typeof json_file === 'undefined') {
            print_help()
        } else {
            fs.readFile(json_file, 'utf8', (err, issue) => {
                if (err) {
                    return console.log(err);
                }
                create(JSON.parse(issue))
            });
        }
        break;

    case "update":
        json_file = args[1]
        if (typeof json_file === 'undefined') {
            print_help()
        } else {
            fs.readFile(json_file, 'utf8', (err, issue) => {
                if (err) {
                    return console.log(err);
                }
                const issue_obj = JSON.parse(issue)
                update(issue_obj.id, issue_obj)
            });
        }
        break;

    case "delete":
        id = args[1]
        if (typeof id === 'undefined') {
            print_help()
        } else {
            del(id)
        }
        break;

    default:
        print_help(false)
}



function print_help(error = true) {
    console.log(HELP)
    if (error) {
        process.exitCode = 1
    }
}

function read(id) {
    axios
        .get(`${REST_URL}/issues/${id}`)
        .then(res => console.log(res.data))
        .catch(error => console.error(`${error}`))
}

function create(issue) {
    axios
        .post(`${REST_URL}/issues/`, issue)
        .then(res => console.log(res.data))
        .catch(error => console.error(`${error}`))
}

function update(issue_id, issue) {
    axios
        .put(`${REST_URL}/issues/${issue_id}`, issue)
        .then(res => console.log(res.data))
        .catch(error => console.error(`${error}`))
}

function del(issue_id) {
    axios
        .delete(`${REST_URL}/issues/${issue_id}`)
        .then(res => console.log(res.data))
        .catch(error => console.error(`${error}`))
}